+++
Description = ""
Tags = ["zsh", "iterm", "fswd"]
date = "2017-01-31T22:44:13-05:00"
title = "Multiple Zsh Configs"
[menu.main]

+++
# Why?

I wanted to maintain multiple configurations for zsh: one for my day
job, and one for the big screen on the nights that I am teaching. My
goal was to make the school shell look as close to what my students
would be using, but I didn't want to go so far as to have to keep two
different shell configurations in my brain.

So the changes are mostly cosmetic (muscle memory requires that I keep
my shell aliases if nothing else).

## First Attempts

I figured I would need to create a new iTerm profile (maybe there was
a better starting point?) so I created one called `Class` and I poked
around with setting an environment variable with the shell as well as
trying plain bash (which is what the students would be using).

## Help from iTerm

While I was experimenting with how to do this in iTerm, I noticed that
the current iTerm profile is reflected in the shell environment
variables, so I decided to try and branch my configuration based on
that.


```zsh
# ~/.zshenv

# default to, well, "default"
export ZSH_PROFILE=${${ITERM_PROFILE:=default}:l}

[[ -f "${ZSH_DIR}/profiles/${ZSH_PROFILE}.env.zsh" ]] &&
    source "${ZSH_DIR}/profiles/${ZSH_PROFILE}.env.zsh"
```

And I use it to set things like the default editor; I use Atom for class.

```zsh
# File: ~/.zsh/profiles/class.env.zsh

export EDITOR="atom -w"
export IN_CLASS

```

While I keep my work configuration separate:

```zsh
# ~/.zsh/profiles/default.env.zsh

export EDITOR="emacsclient -nw"
```

I also used the same technique in the `~/.zshrc` file to split out configurations between work and class

```zsh
# ~/.zshrc
[[ -f "${ZSH_DIR}/profiles/${ZSH_PROFILE}.rc.zsh" ]] &&
    source "${ZSH_DIR}/profiles/${ZSH_PROFILE}.rc.zsh"
```

I plan to split out the prompts as well. I can at least make the class
prompt *look* like the `bash` prompt my students will see.
