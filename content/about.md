+++
date = "2016-12-05T20:07:19-05:00"
title = "about"
type = "page"
[menu.main]

+++


# David Raynes

I am a principal software engineer for Aol. I also teach the [Introduction to Full Stack Web Development](https://betamore.com/academy/full-stack-web-development/) class at Betamore.